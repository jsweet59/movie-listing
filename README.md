# Movie Listing
1. Install Vue CLI:
yarn global add @vue/cli

2. clone the repo:
git clone https://jsweet59@bitbucket.org/jsweet59/movie-listing.git

3. cd into project dir

4. install packages:
yarn install

4. serve it up:
yarn serve

5. visit here in Google Chrome (you didn't expect me to cross-browser test it did you!):
http://localhost:8080/
